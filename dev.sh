python -m pip install --upgrade build
python -m pip install --upgrade twine

python -m build --sdist --wheel && \
twine check dist/load-shedding-0.12.1.tar.gz && \
twine upload dist/load-shedding-0.12.1.tar.gz --verbose