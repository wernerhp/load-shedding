#      A python library for getting Load Shedding schedules.
#      Copyright (C) 2021  Werner Pieterson
#
#      This program is free software: you can redistribute it and/or modify
#      it under the terms of the GNU General Public License as published by
#      the Free Software Foundation, either version 3 of the License, or
#      (at your option) any later version.
#
#      This program is distributed in the hope that it will be useful,
#      but WITHOUT ANY WARRANTY; without even the implied warranty of
#      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#      GNU General Public License for more details.
#
#      You should have received a copy of the GNU General Public License
#      along with this program.  If not, see <https://www.gnu.org/licenses/>.
import json
import logging
import os
from datetime import timezone, timedelta
from json import JSONDecodeError
from typing import Dict

from load_shedding.libs.coct import parse_info

SAST = timezone(timedelta(hours=+2), "SAST")


def get_info() -> Dict:
    try:
        data = []
        __location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
        with open(os.path.join(__location__, "coct_info.json"), "r") as coct_info:
            data = coct_info.read()
    except (FileNotFoundError, JSONDecodeError) as e:
        logging.debug(f"Unable to get schedule from cache. %s", e, exc_info=True)
    else:
        return parse_info(data)
