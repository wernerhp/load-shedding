#      A python library for getting Load Shedding schedules.
#      Copyright (C) 2021  Werner Pieterson
#
#      This program is free software: you can redistribute it and/or modify
#      it under the terms of the GNU General Public License as published by
#      the Free Software Foundation, either version 3 of the License, or
#      (at your option) any later version.
#
#      This program is distributed in the hope that it will be useful,
#      but WITHOUT ANY WARRANTY; without even the implied warranty of
#      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#      GNU General Public License for more details.
#
#      You should have received a copy of the GNU General Public License
#      along with this program.  If not, see <https://www.gnu.org/licenses/>.

from datetime import datetime, timezone, timedelta
from typing import Dict


def get_stage_forecast() -> Dict:
    sast = timezone(timedelta(seconds=7200), 'SAST')
    return [{
        'start_time': datetime(2022, 7, 19, 16, 0, tzinfo=sast),
        'end_time': datetime(2022, 7, 20, 0, 0, tzinfo=sast),
        'stage': 2
    }, {
        'start_time': datetime(2022, 7, 20, 16, 0, tzinfo=sast),
        'end_time': datetime(2022, 7, 21, 0, 0, tzinfo=sast),
        'stage': 2
    }, {
        'start_time': datetime(2022, 7, 21, 16, 0, tzinfo=sast),
        'end_time': datetime(2022, 7, 22, 0, 0, tzinfo=sast),
        'stage': 2
    }, {
        'start_time': datetime(2022, 7, 22, 16, 0, tzinfo=sast),
        'end_time': datetime(2022, 7, 23, 0, 0, tzinfo=sast),
        'stage': 1
    }, {
        'start_time': datetime(2022, 7, 23, 16, 0, tzinfo=sast),
        'end_time': datetime(2022, 7, 24, 0, 0, tzinfo=sast),
        'stage': 1
    }, {
        'start_time': datetime(2022, 7, 22, 16, 0, tzinfo=sast),
        'end_time': datetime(2022, 7, 23, 0, 0, tzinfo=sast),
        'stage': 1
    }, {
        'start_time': datetime(2022, 7, 25, 16, 0, tzinfo=sast),
        'end_time': datetime(2022, 7, 26, 0, 0, tzinfo=sast),
        'stage': 1
    }]
