#      A python library for getting Load Shedding schedules.
#      Copyright (C) 2021  Werner Pieterson
#
#      This program is free software: you can redistribute it and/or modify
#      it under the terms of the GNU General Public License as published by
#      the Free Software Foundation, either version 3 of the License, or
#      (at your option) any later version.
#
#      This program is distributed in the hope that it will be useful,
#      but WITHOUT ANY WARRANTY; without even the implied warranty of
#      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#      GNU General Public License for more details.
#
#      You should have received a copy of the GNU General Public License
#      along with this program.  If not, see <https://www.gnu.org/licenses/>.
import os
import unittest
from unittest import TestCase

from load_shedding.libs import sepush
from tests.libs import mock_sepush

skip_slow = os.environ.get('skip_slow', False)
token = os.environ.get('SEPUSH_TOKEN', "")


@unittest.skipIf(skip_slow, "Slow (Network calls)")
class TestSePush(TestCase):

    def setUp(self) -> None:
        if token:
            self.sepush = sepush.SePush(token)
        else:
            self.sepush = mock_sepush.SePush()

    def test_areas_search(self):
        data = self.sepush.areas_search(text="Milnerton")
        self.assertIsInstance(data, dict)

        self.assertIn("areas", data)
        areas = data.get("areas", [])
        self.assertIsInstance(areas, list)
        for area in areas:
            self.assertIn("id", area)
            self.assertIn("name", area)
            self.assertIn("region", area)

    def test_area(self):
        data = self.sepush.area(area_id="eskde-14-milnertoncityofcapetownwesterncape")
        self.assertIsInstance(data, dict)

        self.assertIn("events", data)
        self.assertIsInstance(data.get("events"), list)
        self.assertIn("info", data)
        self.assertIsInstance(data.get("info"), dict)

        self.assertIn("schedule", data)
        schedule = data.get("schedule")
        self.assertIsInstance(schedule, dict)

        self.assertIn("days", schedule, {})
        days = schedule.get("days", [])
        self.assertIsInstance(days, list)
        for day in days:
            self.assertIn("date", day)
            self.assertIn("name", day)

            self.assertIn("stages", day)
            stages = day.get("stages", [])
            for stage in stages:
                self.assertIsInstance(stage, list)

        self.assertIn("source", schedule)

        data = self.sepush.area(area_id="capetown-13-edgemead")
        self.assertIsInstance(data, dict)

        data = self.sepush.area(area_id="jhbcitypower2-14-blairgowrie")
        self.assertIsInstance(data, dict)

    def test_check_allowance(self):
        data = self.sepush.check_allowance()
        self.assertIsInstance(data, dict)

        self.assertTrue("allowance" in data)
        allowance = data.get("allowance")
        self.assertIsInstance(allowance, dict)

        self.assertTrue("count" in allowance)
        self.assertIsInstance(allowance.get("count"), int)

        self.assertTrue("limit" in allowance)
        limit = allowance.get("limit")
        self.assertIsInstance(limit, int)
        self.assertEqual(limit, 50)

        self.assertTrue("type" in allowance)
        type = allowance.get("type")
        self.assertIsInstance(type, str)
        self.assertEqual(type, "daily")

    def test_status(self):
        data = self.sepush.status()
        self.assertIsInstance(data, dict)

        self.assertIn("status", data)
        status = data.get("status")
        self.assertIsInstance(status, dict)
        self.assertIn("capetown", status)
        self.assertIn("eskom", status)
        for provider in status:
            provider = status.get(provider)
            self.assertIn("name", provider)
            self.assertIn("next_stages", provider)
            self.assertIn("stage", provider)
            self.assertIn("stage_updated", provider)
