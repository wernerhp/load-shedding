#      A python library for getting Load Shedding schedules.
#      Copyright (C) 2021  Werner Pieterson
#
#      This program is free software: you can redistribute it and/or modify
#      it under the terms of the GNU General Public License as published by
#      the Free Software Foundation, either version 3 of the License, or
#      (at your option) any later version.
#
#      This program is distributed in the hope that it will be useful,
#      but WITHOUT ANY WARRANTY; without even the implied warranty of
#      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#      GNU General Public License for more details.
#
#      You should have received a copy of the GNU General Public License
#      along with this program.  If not, see <https://www.gnu.org/licenses/>.
import os
import unittest
from datetime import datetime
from unittest import TestCase

from load_shedding.libs import citypower

skip_slow = os.environ.get('skip_slow', False)


@unittest.skipIf(skip_slow, "Slow (Network calls)")
class TestCityPower(TestCase):

    def test_get_stage_forecast(self):
        stage_forecast = citypower.get_stage_forecast()
        self.assertIsInstance(stage_forecast, list)
        self.assertIsInstance(stage_forecast[0].get("start_time"), datetime)
        self.assertIsInstance(stage_forecast[0].get("end_time"), datetime)
        self.assertIsInstance(stage_forecast[0].get("stage"), int)
