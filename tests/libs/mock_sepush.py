#      A python library for getting Load Shedding schedules.
#      Copyright (C) 2021  Werner Pieterson
#
#      This program is free software: you can redistribute it and/or modify
#      it under the terms of the GNU General Public License as published by
#      the Free Software Foundation, either version 3 of the License, or
#      (at your option) any later version.
#
#      This program is distributed in the hope that it will be useful,
#      but WITHOUT ANY WARRANTY; without even the implied warranty of
#      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#      GNU General Public License for more details.
#
#      You should have received a copy of the GNU General Public License
#      along with this program.  If not, see <https://www.gnu.org/licenses/>.
import json
import logging
import os
from json import JSONDecodeError
from typing import Dict, List, Any

from load_shedding.libs import sepush


class SePush(sepush.SePush):
    __location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
    base_url = f"{__location__}/sepush"

    def areas_search(self, text: str) -> Dict:
        url = f"{self.base_url}/areas_search?text={text}.json"
        data = _call(url, self.token)
        return json.loads(data)

    def area(self, area_id: str) -> Dict:
        url = f"{self.base_url}/area?id={area_id}.json"
        data = _call(url, self.token)
        return json.loads(data)

    def check_allowance(self) -> Dict:
        url = f"{self.base_url}/api_allowance.json"
        data = _call(url, self.token)
        return json.loads(data)

    def status(self) -> Dict:
        url = f"{self.base_url}/status.json"
        data = _call(url, self.token)
        return json.loads(data)


def _call(url: str, token: str) -> Any:
    try:
        data = []
        with open(url, "r") as data:
            data = data.read()
    except (FileNotFoundError, JSONDecodeError) as e:
        logging.debug(f"Unable load mock from %s %s", url, e, exc_info=True)
    else:
        return data
