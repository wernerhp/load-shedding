#      A python library for getting Load Shedding schedules.
#      Copyright (C) 2021  Werner Pieterson
#
#      This program is free software: you can redistribute it and/or modify
#      it under the terms of the GNU General Public License as published by
#      the Free Software Foundation, either version 3 of the License, or
#      (at your option) any later version.
#
#      This program is distributed in the hope that it will be useful,
#      but WITHOUT ANY WARRANTY; without even the implied warranty of
#      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#      GNU General Public License for more details.
#
#      You should have received a copy of the GNU General Public License
#      along with this program.  If not, see <https://www.gnu.org/licenses/>.

from datetime import datetime, timezone, timedelta
from typing import Dict, List, Tuple

from load_shedding.libs import eskom
from load_shedding.providers import to_utc


class LoadShedding(eskom.LoadShedding):
    base_url = "http://mock"

    def find_suburbs(self, search_text: str, max_results: int = 10) -> List[Dict]:
        return [{
            'Id': 1058852,
            'MunicipalityName': 'City of Cape Town',
            'Name': 'Milnerton',
            'ProvinceName': 'Western Cape',
            'Total': 704
        }, {
            'Id': 1058853,
            'MunicipalityName': 'City of Cape Town',
            'Name': 'Milnerton Golf Course',
            'ProvinceName': 'Western Cape',
            'Total': 0
        }, {
            'Id': 1058854,
            'MunicipalityName': 'City of Cape Town',
            'Name': 'Milnerton Outlying',
            'ProvinceName': 'Western Cape',
            'Total': 0
        }, {
            'Id': 1058855,
            'MunicipalityName': 'City of Cape Town',
            'Name': 'Milnerton Ridge',
            'ProvinceName': 'Western Cape',
            'Total': 704
        }, {
            'Id': 1058856,
            'MunicipalityName': 'City of Cape Town',
            'Name': 'Milnerton SP',
            'ProvinceName': 'Western Cape',
            'Total': 2816
        }, {
            'Id': 1069144,
            'MunicipalityName': 'City of Cape Town',
            'Name': 'Milnerton SP 1',
            'ProvinceName': 'Western Cape',
            'Total': 3520
        }, {
            'Id': 1069145,
            'MunicipalityName': 'City of Cape Town',
            'Name': 'Milnerton SP 2',
            'ProvinceName': 'Western Cape',
            'Total': 700
        }]

    def get_municipalities(self, province: eskom.Province) -> List[Dict]:
        return [
            {'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Beaufort West', 'Value': '336'},
            {'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Bergrivier', 'Value': '337'},
            {'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Bitou', 'Value': '338'},
            {'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Breede Valley', 'Value': '339'},
            {'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Cape Agulhas', 'Value': '340'},
            {'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Cederberg', 'Value': '341'},
            {'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'City of Cape Town', 'Value': '342'},
            {'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Drakenstein', 'Value': '343'},
            {'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'George', 'Value': '344'},
            {'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Hessequa', 'Value': '345'},
            {'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Kannaland', 'Value': '346'},
            {'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Knysna', 'Value': '347'},
            {'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Laingsburg', 'Value': '348'},
            {'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Langeberg', 'Value': '349'},
            {'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Matzikama', 'Value': '350'},
            {'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Mossel Bay', 'Value': '351'},
            {'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Oudtshoorn', 'Value': '352'},
            {'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Overstrand', 'Value': '353'},
            {'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Prince Albert', 'Value': '354'},
            {'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Saldanha Bay', 'Value': '355'},
            {'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Stellenbosch', 'Value': '356'},
            {'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Swartland', 'Value': '357'},
            {'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Swellendam', 'Value': '358'},
            {'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Theewaterskloof', 'Value': '359'},
            {'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Witzenberg', 'Value': '360'}
        ]

    def get_schedule(self, province: eskom.Province, suburb_id: int, stage: int) -> List[Tuple]:
        sast = timezone(timedelta(hours=+2), 'SAST')
        schedule = {
            1: [
                (datetime(2022, 7, 19, 2, 0, tzinfo=sast), datetime(2022, 7, 19, 4, 30, tzinfo=sast)),
                (datetime(2022, 7, 20, 10, 0, tzinfo=sast), datetime(2022, 7, 20, 12, 30, tzinfo=sast)),
                (datetime(2022, 7, 21, 16, 0, tzinfo=sast), datetime(2022, 7, 21, 18, 30, tzinfo=sast)),
                (datetime(2022, 7, 23, 0, 0, tzinfo=sast), datetime(2022, 7, 23, 2, 30, tzinfo=sast)),
                (datetime(2022, 7, 24, 8, 0, tzinfo=sast), datetime(2022, 7, 24, 10, 30, tzinfo=sast)),
                (datetime(2022, 7, 25, 14, 0, tzinfo=sast), datetime(2022, 7, 25, 16, 30, tzinfo=sast)),
                (datetime(2022, 7, 26, 22, 0, tzinfo=sast), datetime(2022, 7, 27, 0, 30, tzinfo=sast)),
                (datetime(2022, 7, 28, 6, 0, tzinfo=sast), datetime(2022, 7, 28, 8, 30, tzinfo=sast)),
                (datetime(2022, 7, 29, 12, 0, tzinfo=sast), datetime(2022, 7, 29, 14, 30, tzinfo=sast)),
            ],
            2: [
                (datetime(2022, 7, 19, 2, 0, tzinfo=sast), datetime(2022, 7, 19, 4, 30, tzinfo=sast)),
                (datetime(2022, 7, 20, 2, 0, tzinfo=sast), datetime(2022, 7, 20, 4, 30, tzinfo=sast)),
                (datetime(2022, 7, 20, 10, 0, tzinfo=sast), datetime(2022, 7, 20, 12, 30, tzinfo=sast)),
                (datetime(2022, 7, 21, 8, 0, tzinfo=sast), datetime(2022, 7, 21, 10, 30, tzinfo=sast)),
                (datetime(2022, 7, 21, 16, 0, tzinfo=sast), datetime(2022, 7, 21, 18, 30, tzinfo=sast)),
                (datetime(2022, 7, 22, 16, 0, tzinfo=sast), datetime(2022, 7, 22, 18, 30, tzinfo=sast)),
                (datetime(2022, 7, 23, 0, 0, tzinfo=sast), datetime(2022, 7, 23, 2, 30, tzinfo=sast)),
                (datetime(2022, 7, 24, 0, 0, tzinfo=sast), datetime(2022, 7, 24, 2, 30, tzinfo=sast)),
                (datetime(2022, 7, 24, 8, 0, tzinfo=sast), datetime(2022, 7, 24, 10, 30, tzinfo=sast)),
                (datetime(2022, 7, 25, 6, 0, tzinfo=sast), datetime(2022, 7, 25, 8, 30, tzinfo=sast)),
                (datetime(2022, 7, 25, 14, 0, tzinfo=sast), datetime(2022, 7, 25, 16, 30, tzinfo=sast)),
                (datetime(2022, 7, 26, 14, 0, tzinfo=sast), datetime(2022, 7, 26, 16, 30, tzinfo=sast)),
                (datetime(2022, 7, 26, 22, 0, tzinfo=sast), datetime(2022, 7, 27, 0, 30, tzinfo=sast)),
                (datetime(2022, 7, 27, 22, 0, tzinfo=sast), datetime(2022, 7, 28, 0, 30, tzinfo=sast)),
                (datetime(2022, 7, 28, 6, 0, tzinfo=sast), datetime(2022, 7, 28, 8, 30, tzinfo=sast)),
                (datetime(2022, 7, 29, 4, 0, tzinfo=sast), datetime(2022, 7, 29, 6, 30, tzinfo=sast)),
            ],
        }.get(stage)

        return to_utc(schedule)

    def get_schedule_area_info(self, suburb_id: int) -> Dict:
        return {
            'Province': {'Id': 9, 'Name': 'Western Cape'},
            'Municipality': {'Id': 342, 'Name': 'City of Cape Town'},
            'Suburb': {'Id': 1058852, 'Name': 'Milnerton'},
            'Period': ['02-07-2022', '29-07-2022']
        }

    def get_status(self) -> int:
        return 3  # Stage 2
