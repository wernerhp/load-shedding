#      A python library for getting Load Shedding schedules.
#      Copyright (C) 2021  Werner Pieterson
#
#      This program is free software: you can redistribute it and/or modify
#      it under the terms of the GNU General Public License as published by
#      the Free Software Foundation, either version 3 of the License, or
#      (at your option) any later version.
#
#      This program is distributed in the hope that it will be useful,
#      but WITHOUT ANY WARRANTY; without even the implied warranty of
#      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#      GNU General Public License for more details.
#
#      You should have received a copy of the GNU General Public License
#      along with this program.  If not, see <https://www.gnu.org/licenses/>.
import os
import unittest
from unittest import TestCase

from load_shedding.libs.eskom import LoadShedding, Province

skip_slow = os.environ.get('skip_slow', False)


@unittest.skipIf(skip_slow, "Slow (Network calls)")
class TestEskom(TestCase):

    def setUp(self) -> None:
        self.eskom = LoadShedding()

    def test_find_suburbs(self):
        suburbs = self.eskom.find_suburbs(search_text="Milnerton")
        self.assertEqual(list, type(suburbs))
        self.assertNotEqual(0, len(suburbs))
        self.assertEqual(dict, type(suburbs[0]))
        self.assertEqual(1058852, suburbs[0]["Id"])
        self.assertEqual("City of Cape Town", suburbs[0]["MunicipalityName"])
        self.assertEqual("Milnerton", suburbs[0]["Name"])
        self.assertEqual(str(Province.WESTERN_CAPE), suburbs[0]["ProvinceName"])
        self.assertEqual(int, type(suburbs[0]["Total"]))

    def test_get_municipalities(self):
        municipalities = self.eskom.get_municipalities(province=Province.WESTERN_CAPE)
        self.assertEqual(list, type(municipalities))
        self.assertNotEqual(0, len(municipalities))
        self.assertEqual(dict, type(municipalities[0]))
        self.assertEqual("Beaufort West", municipalities[0]["Text"])

    def test_get_schedule(self):
        schedule = self.eskom.get_schedule(province=Province.WESTERN_CAPE, suburb_id=1058852, stage=2)
        self.assertEqual(list, type(schedule))
        self.assertNotEqual(0, len(schedule))
        self.assertEqual(tuple, type(schedule[0]))
        self.assertNotEqual(0, len(schedule[0]))

    def test_get_schedule_area_info(self):
        area_info = self.eskom.get_schedule_area_info(suburb_id=1058852)
        self.assertEqual(dict, type(area_info))
        self.assertNotEqual(0, len(area_info))
        self.assertEqual(Province.WESTERN_CAPE, Province(area_info["Province"]["Id"]))
        self.assertEqual(str(Province.WESTERN_CAPE), area_info["Province"]["Name"])
        self.assertEqual(1058852, area_info["Suburb"]["Id"])
        self.assertEqual("Milnerton", area_info["Suburb"]["Name"])
        self.assertEqual(342, area_info["Municipality"]["Id"])
        self.assertEqual("City of Cape Town", area_info["Municipality"]["Name"])
        self.assertEqual(list, type(area_info["Period"]))

    def test_get_status(self):
        status = self.eskom.get_status()
        self.assertEqual(int, type(status))
