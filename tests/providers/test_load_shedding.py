#      A python library for getting Load Shedding schedules.
#      Copyright (C) 2021  Werner Pieterson
#
#      This program is free software: you can redistribute it and/or modify
#      it under the terms of the GNU General Public License as published by
#      the Free Software Foundation, either version 3 of the License, or
#      (at your option) any later version.
#
#      This program is distributed in the hope that it will be useful,
#      but WITHOUT ANY WARRANTY; without even the implied warranty of
#      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#      GNU General Public License for more details.
#
#      You should have received a copy of the GNU General Public License
#      along with this program.  If not, see <https://www.gnu.org/licenses/>.
import os
import unittest
from datetime import datetime, timezone
from unittest import TestCase

from load_shedding import (
    Area, Provider, Province, Stage, StageError,
    get_areas, get_area_schedule, get_stage, get_stage_forecast, get_area_forecast, providers
)
from load_shedding.load_shedding import list_to_dict
from tests.providers import mock_eskom

skip_slow = os.environ.get('skip_slow', False)


@unittest.skipIf(skip_slow, "Slow (Network calls)")
class Test(TestCase):
    def setUp(self) -> None:
        self.provider = mock_eskom.Eskom()

    def test_get_stage(self):
        got = get_stage(self.provider)
        self.assertEqual(Stage.STAGE_2, got)

    def test_get_stage_forecast(self):
        got = get_stage_forecast(self.provider)
        self.assertIsInstance(got, list)
        self.assertIsInstance(got[0].get("stage"), Stage)
        self.assertIsInstance(got[0].get("start_time"), datetime)
        self.assertIsInstance(got[0].get("end_time"), datetime)

    def test_get_area_forecast(self):
        utc = timezone.utc
        now = datetime(2022, 7, 19, 0, 0, tzinfo=utc)

        planned_stages = get_stage_forecast(self.provider)
        area_forecast = []
        for planned in planned_stages:
            forecast_stage = planned.get("stage")
            area_schedule = get_area_schedule(self.provider, area=Area(id=1058852, province=Province.WESTERN_CAPE),
                                              stage=forecast_stage, cached=False)

            af = get_area_forecast(area_schedule=area_schedule, planned=planned, now=now)
            self.assertIsInstance(af, list)
            area_forecast.extend(af)

        for planned in area_forecast:
            self.assertIsInstance(planned, dict)
            self.assertIsInstance(planned.get("stage"), Stage)
            self.assertIsInstance(planned.get("start_time"), datetime)
            self.assertIsInstance(planned.get("end_time"), datetime)

        want = [{
            'stage': Stage.STAGE_2,
            'start_time': datetime(2022, 7, 21, 14, 00, tzinfo=utc),
            'end_time': datetime(2022, 7, 21, 16, 30, tzinfo=utc)
        }, {
            'stage': Stage.STAGE_1,
            'start_time': datetime(2022, 7, 25, 14, 0, tzinfo=utc),
            'end_time': datetime(2022, 7, 25, 14, 30, tzinfo=utc)
        }]
        self.assertEqual(want, area_forecast)

    def test_get_area_schedule(self):
        self.assertRaises(StageError, get_area_schedule, self.provider,
                          area=Area(id=1058852, province=Province.WESTERN_CAPE), stage=Stage.Stage.NO_LOAD_SHEDDING, cached=False)
        utc = timezone.utc
        want = [
            (datetime(2022, 7, 19, 0, 0, tzinfo=utc), datetime(2022, 7, 19, 2, 30, tzinfo=utc)),
            (datetime(2022, 7, 20, 0, 0, tzinfo=utc), datetime(2022, 7, 20, 2, 30, tzinfo=utc)),
            (datetime(2022, 7, 20, 8, 0, tzinfo=utc), datetime(2022, 7, 20, 10, 30, tzinfo=utc)),
            (datetime(2022, 7, 21, 6, 0, tzinfo=utc), datetime(2022, 7, 21, 8, 30, tzinfo=utc)),
            (datetime(2022, 7, 21, 14, 0, tzinfo=utc), datetime(2022, 7, 21, 16, 30, tzinfo=utc)),
            (datetime(2022, 7, 22, 14, 0, tzinfo=utc), datetime(2022, 7, 22, 16, 30, tzinfo=utc)),
            (datetime(2022, 7, 22, 22, 0, tzinfo=utc), datetime(2022, 7, 23, 0, 30, tzinfo=utc)),
            (datetime(2022, 7, 23, 22, 0, tzinfo=utc), datetime(2022, 7, 24, 0, 30, tzinfo=utc)),
            (datetime(2022, 7, 24, 6, 0, tzinfo=utc), datetime(2022, 7, 24, 8, 30, tzinfo=utc)),
            (datetime(2022, 7, 25, 4, 0, tzinfo=utc), datetime(2022, 7, 25, 6, 30, tzinfo=utc)),
            (datetime(2022, 7, 25, 12, 0, tzinfo=utc), datetime(2022, 7, 25, 14, 30, tzinfo=utc)),
            (datetime(2022, 7, 26, 12, 0, tzinfo=utc), datetime(2022, 7, 26, 14, 30, tzinfo=utc)),
            (datetime(2022, 7, 26, 20, 0, tzinfo=utc), datetime(2022, 7, 26, 22, 30, tzinfo=utc)),
            (datetime(2022, 7, 27, 20, 0, tzinfo=utc), datetime(2022, 7, 27, 22, 30, tzinfo=utc)),
            (datetime(2022, 7, 28, 4, 0, tzinfo=utc), datetime(2022, 7, 28, 6, 30, tzinfo=utc)),
            (datetime(2022, 7, 29, 2, 0, tzinfo=utc), datetime(2022, 7, 29, 4, 30, tzinfo=utc)),
        ]

        got = get_area_schedule(self.provider, area=Area(id=1058852, province=Province.WESTERN_CAPE), cached=False)
        self.assertIsInstance(got, list)
        self.assertIsInstance(got[0], tuple)
        self.assertListEqual(want, got)

        got = get_area_schedule(self.provider, area=Area(id=1058852, province=Province.WESTERN_CAPE),
                                stage=Stage.STAGE_2,
                                cached=False)
        self.assertIsInstance(got, list)
        self.assertIsInstance(got[0], tuple)
        self.assertListEqual(want, got)

    def test_get_areas(self):
        areas = get_areas(self.provider, search_text="Milnerton")
        self.assertIsInstance(areas, list)
        self.assertEqual(5, len(areas))
        self.assertIsInstance(areas[0], Area)
        self.assertIsInstance(areas[0].province, Province)

    def test_list_to_dict(self):
        schedule = get_area_schedule(self.provider, area=Area(id=1058852, province=Province.WESTERN_CAPE),
                                     stage=Stage.STAGE_2)
        self.assertIsInstance(schedule, list)
        got = list_to_dict(schedule)
        self.assertIsInstance(got, dict)

    def test_providers(self):
        for provider in Provider:
            self.assertIsInstance(provider, Provider)
            self.assertIsInstance(provider.name, str)
            self.assertIsInstance(provider.value, int)
            self.assertIsInstance(provider(), providers.Provider)

    def test_province(self):
        for province in Province:
            self.assertIsInstance(province, Province)
            self.assertIsInstance(province.name, str)
            self.assertIsInstance(province.value, int)

        self.assertEqual(Province.WESTERN_CAPE, Province(9))
        self.assertEqual(9, Province(9).value)
        self.assertEqual("Western Cape", str(Province(9)))
