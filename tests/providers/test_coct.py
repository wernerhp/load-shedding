#      A python library for getting Load Shedding schedules.
#      Copyright (C) 2021  Werner Pieterson
#
#      This program is free software: you can redistribute it and/or modify
#      it under the terms of the GNU General Public License as published by
#      the Free Software Foundation, either version 3 of the License, or
#      (at your option) any later version.
#
#      This program is distributed in the hope that it will be useful,
#      but WITHOUT ANY WARRANTY; without even the implied warranty of
#      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#      GNU General Public License for more details.
#
#      You should have received a copy of the GNU General Public License
#      along with this program.  If not, see <https://www.gnu.org/licenses/>.

from datetime import datetime, timezone
from unittest import TestCase

from load_shedding import Area, Province, Stage
from tests.providers.mock_coct import CoCT


class TestCoCT(TestCase):

    def setUp(self) -> None:
        self.provider = CoCT()

    def test_get_areas(self):
        areas = self.provider.get_areas()
        self.assertIsInstance(areas, list)
        self.assertEqual(16, len(areas))
        self.assertIsInstance(areas[0], Area)
        self.assertEqual(1, areas[0].id)
        self.assertIsInstance(areas[0].province, Province)

    def test_get_area_schedule(self):
        schedule = self.provider.get_area_schedule(area=Area(id=13, province=Province.WESTERN_CAPE), stage=Stage.STAGE_4)
        self.assertIsInstance(schedule, list)
        self.assertNotEqual(0, len(schedule))
        self.assertNotEqual(0, len(schedule[0]))
        self.assertIsInstance(schedule[0][0], datetime)
        self.assertEqual(schedule[0][0].tzinfo, timezone.utc)

    def test_get_stage(self):
        stage = self.provider.get_stage()
        self.assertEqual(Stage, type(stage))
        self.assertEqual(Stage.NO_LOAD_SHEDDING, stage)

    def test_get_stage_forecast(self):
        got = self.provider.get_stage_forecast()
        self.assertIsInstance(got, list)
        self.assertIsInstance(got[0].get("stage"), Stage)
        self.assertIsInstance(got[0].get("start_time"), datetime)
